ping:
	ansible all -m ping

# run all playbooks
all:
	ansible-playbook playbooks/add_public_keys.yaml -K && \
	ansible-playbook playbooks/update-server.yaml && \
	ansible-playbook playbooks/install-docker.yaml && \
	ansible-playbook playbooks/nginx_and_ssl_config.yaml

# run playbooks individually
update_server:
	ansible-playbook playbooks/update-server.yaml

add_public_keys:
	ansible-playbook playbooks/add_public_keys.yaml -K

install_docker:
	ansible-playbook playbooks/install-docker.yaml

nginx_and_ssl_config:
	ansible-playbook playbooks/nginx_and_ssl_config.yaml